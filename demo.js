// -------------------------------------- 1 --------------------------------------  //

// arr = ["tomato", "apple", "brinjal"];
// x = "apple";

// :::: SOLUTION ::::

// console.log(arr.includes(x));

// -------------------------------------- 2 --------------------------------------  //

// arr = [1, 2, 3, 4, 5];
// desired = arr.map((i) => i * 10);

// :::: SOLUTION ::::

// console.log(desired);
// desired = [10,20,30,40,50]

// -------------------------------------- 3 --------------------------------------  //

// arr = [2, 83, 90, 73, 29, 98];

// :::: SOLUTION ::::

// console.log(arr.filter((num) => num > 20).length);

// console.log(arr.findIndex((i) => i > 20));

// console.log(arr.copyWithin(1, 3, 5));

// -------------------------------------- 4 --------------------------------------  //

// let data = [
//   { name: "mAnan", age: 50, email: "mailto:xyz@yopmail.com" },
//   { name: false, age: 20, email: "mailto:xyz@yopmail.com" },
//   { name: "SanjaY", age: 10 },
//   { name: false, age: 20, email: "mailto:xyz@yopmail.com" },
//   { name: "RavI", age: 30 },
//   { name: "surendra", age: 15, email: "mailto:xyz@yopmail.com" },
//   { name: "kaMLesH", age: 40, email: "mailto:ccd@yopmail.com" },
//   { name: 22, age: 50 },
//   { name: "LaKSHy", age: 60, email: "mailto:abc@yopmail.com" },
// ];
// // kamlesh and lakshy and manan and ravi

// :::: SOLUTION ::::

// let names = [];
// data.map((item) => {
//   if (typeof item.name === "string" && item.age >= 20) {
//     names.push(item.name.toLowerCase());
//   }
// });

// console.log(names.sort().reverse().join(" and "));

// -------------------------------------- 5 --------------------------------------  //

// data = [" avengers", "   captain_america", "ironman   ", " black panther   "];

// :::: SOLUTION ::::

// changed = data.map((item) => {
//   return item.toString().trim();
// });

// console.log(changed);
// //["avengers", "captain_america", "ironman", "black panther");

// -------------------------------------- 6 --------------------------------------  //

// Write a function that converts an array of values from miles to kilometres using the map method. In the end, add the kilometres up in a new variable called "totalDistanceInKilometers" and return this variable. (1 mile = 1.5 kilometres)]

// let distance = [10, 20, 30, 40, 50];

// :::: SOLUTION ::::

// let kilos = distance.reduce((total, d) => {
//   console.log(total, d);
//   return total + d * 1.609;
// }, 0);
// console.log(kilos);

// -------------------------------------- 7 --------------------------------------  //

// let nums = [2, 3, 4, 5, 6, 7, 8, 9];

// :::: SOLUTION ::::

// let newArr = nums.map((n) => n + 4);

// let squares = nums.map((n) => n * n);
// console.log(squares);

// let sum = squares.reduce((total, value) => (total = total + value));
// console.log(sum / nums.length);

// console.log(newArr);

// -------------------------------------- 8 --------------------------------------  //

// let nums = [2, 3, 4, 5, 6, 7, 8, 9];

// :::: SOLUTION ::::

// let even = [];
// even = nums.filter((n) => n % 2 == 0);

// let squares = even.map((n) => {
//   return n * n;
// });

// let sub = squares.reduce((total, num) => total - num);
// console.log(even, squares, sub);

// -------------------------------------- 9 --------------------------------------  //

// Create a new array whose elements is in uppercase of words present in the original array.

// arr = ["vrunda", "manan"];

// let caps = arr.map((cap) => cap[0].toUpperCase().concat(cap.substr(1)));

// console.log(caps);

// -------------------------------------- 10 --------------------------------------  //

// Use the .map() method on the heros array to return a new array.

// The new array should rename the 'name' key to 'hero'.
// The 'name' key should not appear in the new array.
// The new array should have a new key added called (id).
// The key 'id' should be based on the index.

// const heros = [
//   { name: "Spider-Man" },
//   { name: "Thor" },
//   { name: "Black Panther" },
//   { name: "Captain Marvel" },
//   { name: "Silver Surfer" },
// ];
// // EXPECTED OUTPUT (array of objects):
// // [
// //   { id: 0, hero: 'Spider-Man' },
// //   { id: 1, hero: 'Thor' },
// //   { id: 2, hero: 'Black Panther' },
// //   { id: 3, hero: 'Captain Marvel' },
// //   { id: 4, hero: 'Silver Surfer' }
// // ]

// let res = heros.map((hero, index) => {
//   return { id: index, hero: hero.name };
// });

// console.log(res);

// -------------------------------------- 10 --------------------------------------  //

// const inputWords = [
//   "spray",
//   "limit",
//   "elite",
//   "limit",
//   "elite",
//   "elite",
//   "limit",
//   "elite",
//   "elite",
//   "elite",
//   "exuberant",
//   "destruction",
//   "present",
// ];
// // Write JavaScript statements that will produce the following output:

// // ["exuberant", "destruction", "present"]
// let inputs = inputWords.slice(-3);
// // let inputs = inputWords.filter((input, index) => index > inputWords.length - 4);

// console.log(inputs);

// -------------------------------------- 11 --------------------------------------  //

// Starting with an array containing the numbers 1 through 10, use filter, map, and reduce to produce the following. Use console.log to display the results.

// An array of odd numbers.
// An array of numbers divisible by 2 or 5.
// An array of numbers divisible by 3 and then squared those numbers.
// The sum of the following: square the numbers divisible by 5.

// numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// console.log(numbers.filter((num) => num % 2 == 1));
// console.log(numbers.filter((num) => num % 2 == 0 || num % 5 == 0));
// console.log(numbers.filter((num) => num % 3 == 0).map((n) => n * n));
// console.log(
//   numbers
//     .filter((num) => num % 5 == 0)
//     // .map((n) => n * n)
//     .reduce((total, value) => total + value * value, 0)
// );

// -------------------------------------- 12 --------------------------------------  //

// let nums = [11, 22, 33, 46, 75, 86, 97, 98];
// // Use filter then map functions to filter even numbers then square them. Assign the result to a variable named squaredEvenNums and display it. The output should be:

// // squaredEvenNums: [484, 2116, 7396, 9604]
// // Use the reduce function to calculate the sum of nums array. The output should be:

// // Sum of array elements: 468

// let squaredEvenNums = nums.filter((n) => n % 2 == 0).map((num) => num * num);
// console.log(nums.reduce((total, num) => (total += num)));

// console.log(squaredEvenNums);

// -------------------------------------- 13 --------------------------------------  //

// Write a function Myfunc that takes in an array of numbers and multiply each of the elements by 2.

// Sample Input
// Myfunc([1, 2 ,3]);
// Sample Output
// [2, 4, 6]

// let dummy = [1, 2, 3];

// myFunc = (arr) => {
//   return arr.map((n) => n * 2);
// };
// console.log(myFunc(dummy));

// -------------------------------------- 14 --------------------------------------  //

// Write a function called displayName() that takes an object as an argument and prints the person's first and last name. Use object destructuring in the function argument. And also, use template strings when printing the first and last name.

// const person = {
//   first: "Elon",
//   last: "Musk",
//   twitter: "@elonmusk",
//   company: "Space X",
// };

// displayName = ({ first, last }) => {
//   console.log(`FirstName: ${first} LasteName: ${last}`);
// };

// displayName(person);

// -------------------------------------- 15 --------------------------------------  //

// Write a function called calculateSalesTotals() which returns an array with new two new keys (sale and total). The key 'sale' equals the calculated sale price based on the original price and the discount. The key 'total' equals the computed total based on stock, the original price and the discount. You have to use Object Destructuring on the objects in the sales array and Object Default Values for the discount key. The default value would be 0.0.

// sale = original - original*discount
// total= stock*sale

// const sales = [
//   { item: "PS4 Pro", stock: 3, original: 399.99 },
//   { item: "Xbox One X", stock: 1, original: 499.99, discount: 0.1 },
//   { item: "Nintendo Switch", stock: 4, original: 299.99 },
//   { item: "PS2 Console", stock: 1, original: 299.99, discount: 0.8 },
//   { item: "Nintendo 64", stock: 2, original: 199.99, discount: 0.65 },
// ];
// EXPECTED OUTPUT (array of objects):
// [
//   {
//     item: "PS4 Pro",
//     original: 399.99,
//     sale: 399.99,
//     stock: 3,
//     total: 1199.97
//   },
//   {
//     discount: 0.1,
//     item: "Xbox One X",
//     original: 499.99,
//     sale: 449.991,
//     stock: 1,
//     total: 449.991
//   },
//   {
//     item: "Nintendo Switch",
//     original: 299.99,
//     sale: 299.99,
//     stock: 4,
//     total: 1199.96
//   },
//   {
//     discount: 0.8,
//     item: "PS2 Console",
//     original: 299.99,
//     sale: 59.99799999999999,
//     stock: 1,
//     total: 59.99799999999999
//   },
//   {
//     discount: 0.65,
//     item: "Nintendo 64",
//     original: 199.99,
//     sale: 69.9965,
//     stock: 2,
//     total: 139.993
//   }
// ]

// calculateSalesTotals = (arr) => {
//   return arr.map((item, index) => {
//     let { original, stock, discount = 0.0 } = item;
//     let sale = original - original * discount;
//     let total = stock * sale;

//     return { ...item, sale, total };
//   });
// };

// console.log(calculateSalesTotals(sales));

// -------------------------------------- 15 --------------------------------------  //

// Create a function named goToSecondClass() that accepts a destructured object as a parameter. The parameter extracts the "secondHour" property of the object. The function should return this statement:

// "Time to go to {property_value} class!"
// Example: "Time to go to Programming class!"

// You can test your function with this object:

// const myClasses = {
//   firstHour: "Ethics",
//   secondHour: "Programming",
//   thirdHour: "Biology",
// };

// const goToSecondClass = ({ secondHour }) => {
//   return `Time to go to ${secondHour} class!`;
// };

// console.log(goToSecondClass(myClasses));

// // -------------------------------------- 15 --------------------------------------  //

// // Consider the following array:

// let colors = ["white", "blue", "yellow", "black", "red", "green"];
// // Using array destructuring assign the first 2 elements to firstColor and secondColor variables and assign the remaining elements to otherColors variable. Display the values of these 3 variables.

// [firstColor, secondColor, ...otherColors] = colors;

// console.log(firstColor, secondColor, otherColors);

// -------------------------------------- 16 --------------------------------------  //
// Write an arrow function expression called greet(). It should accept a single argument representing a person's name. It should return a greeting string as shown below:

// greet("Hagrid"); //"Hey Hagrid!"
// greet("Luna"); //"Hey Luna!"

// const greet = (name) => {
//   return `Hey, ${name}`;
// };
// console.log(greet("vrunda"));

// -------------------------------------- 17 --------------------------------------  //
// Write an arrow function named arrayAverage that accepts an array of numbers and returns the average of those numbers.

// const arrayAverage = (arr) => {
//   return arr.reduce((total, num) => (total += num)) / arr.length;
// };
// console.log(arrayAverage([1, 2, 3, 4, 5, 6]));

// -------------------------------------- 18 --------------------------------------  //
// Convert the following JavaScript function declaration to arrow function syntax.

// function counterFunc(counter) {
//   if (counter > 100) {
//     counter = 0;
//   }else {
//     counter++;
//   }

//   return counter;
// }

// const counterFunc = (counter) => {
//   if (counter > 100) {
//     counter = 0;
//   } else {
//     counter++;
//   }
//   return counter;
// };

// console.log(counterFunc(190));

// -------------------------------------- 19 --------------------------------------  //
// Write an arrow function for the following JavaScript function:

// const nameAge = (name, age) => {
//   console.log("Hello " + name);
//   console.log("You are " + age + " years old");
// };
// nameAge("vrunda", 20);

// -------------------------------------- 20 --------------------------------------  //

// Write an arrow function named dashTwixt2Evens that accepts a number and inserts dashes (-) between two even numbers.

// Expected Output
// dashTwixt2Evens(8675309) //"8-675309"

// const dashTwixt2Evens = (num) => {
//   newNum = num.toString().split("");

//   let even = newNum.map((n, index) => {
//     if (Number(n) % 2 == 0 && newNum[index + 1] % 2 == 0) {
//       return `${n}-`;
//     } else {
//       return n;
//       return even.join("");
//     }
//   });
// };
// console.log(dashTwixt2Evens(225468)); //"2-254-6-8"

// -------------------------------------- 21 --------------------------------------  //
