// sentense case

function titleCase(str) {
  return str
    .toLowerCase()
    .split(' ')
    .map(function (word) {
      return word.charAt(0).toUpperCase() + word.slice(1);
    })
    .join(' ');
}
const changedText = titleCase("I'm a little tea pot");

console.log(changedText);

// const d1 = new Date(2020, 3, 26);
// const d2 = new Date(2021, 1, 27);

// console.log((d2 - d1) / (1000 * 60 * 60 * 24));
