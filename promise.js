//promises
const usingReddit = true;
const usingInsta = false;

function watchingPromise() {
  return new Promise((resolve, reject) => {
    if (usingReddit) {
      reject({
        name: "Reddit",
        message: "User watching memes on Reddit",
      });
    } else if (usingInsta) {
      reject({
        name: "Instagram",
        message: "User watching memes on Instagram.",
      });
    } else {
      resolve("User watching video on Youtube");
    }
  });
}

watchingPromise()
  .then((message) => {
    console.log("Success " + message);
  })
  .catch((error) => {
    console.log(error.name + " " + error.message);
  });

// let p = new Promise((resolve, reject) => {
//   let a = 1 + 2;
//   if (a == 2) {
//     resolve("Success");
//   } else {
//     reject("failed");
//   }
// });

// p.then((message) => {
//   console.log("this is resolved - " + message);
// }).catch((message) => {
//   console.log("this is rejectd - " + message);
// });
