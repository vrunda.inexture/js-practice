let obj = {
  manan: ["modasa", "filmmaker", "handsome"],
  vrunda: ["dakor", "singer", "pretty"],
};

obj[Symbol.iterator] = function () {
  const objKeys = Object.keys(this);
  let keyIndex = 0;
  let valueindex = 0;

  return {
    next: () => {
      // We already iterated over all cities
      if (keyIndex >= objKeys.length) {
        return {
          value: undefined,
          done: true,
        };
      }

      const values = this[objKeys[keyIndex]];
      const property = values[valueindex];

      const isLastProperty = valueindex >= values.length - 1;

      valueindex += 2;

      if (isLastProperty) {
        valueindex = 0;
        keyIndex++;
      }

      return {
        done: false,
        value: property,
      };
    },
  };
};

for (x of obj) {
  console.log(x);
}
