// This keyword and Arrow function
const person = {
  name: "Vrunda",
  talk() {
    console.log(this);
  },
};

person.talk();

const talk = person.talk;
talk(); //Window object

const talk1 = person.talk.bind(person);
talk1(); //binding with bind()
