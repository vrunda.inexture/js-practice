//async and api

const getUser = async () => {
  let people = await fetch("https://randomuser.me/api/?results=5");

  let data = await people.json();

  const timeline = document.querySelector("#timeline");
  data.results.forEach((user) => {
    let image = document.createElement("img");
    image.src = user.picture.large;
    timeline.appendChild(image);
  });
  console.log(data);
};

getUser();
