function urlify(blogTitle) {
  let regex = /[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]/g;

  const url = blogTitle.replace(regex, "");
  return url.toLowerCase().trim().replace(/ /g, "-");
}

console.log(urlify("Does this work??"));
console.log(urlify("Haven't I told you?"));

