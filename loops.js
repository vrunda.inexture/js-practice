// runs a function once for every item in array
function coffeeBill(coffeeArr) {
  let coffeeSum = coffeeArr.reduce(
    (totalCoffees, numcoffees) => (totalCoffees += numcoffees)
  );
  return `The total bill is: $ ${coffeeSum * 1.25}.`;
}

console.log(coffeeBill([2, 5, 2, 1, 3]));
