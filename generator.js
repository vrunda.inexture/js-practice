function* getJourney() {
  yield "Dakor";
  yield "Kapdvanj";
  yield "Bayad";
  yield "Dhansura";
  yield "Modasa";
}

const bus = getJourney();

const nextStop = document.querySelector("#stop");
nextStop.addEventListener("click", (event) => {
  let currStop = bus.next();
  if (currStop.done) {
    console.log("We made it!");
    nextStop.setAttribute("disabled", true);
  } else {
    console.log(currStop.value);
  }
});

/** #generator Function
 * doesn't execute the body immediately
 * returns an iterator
 * call next to hit the next value
 * next return object with value property
 * exited and reentered at later point of time with the same context
 */
