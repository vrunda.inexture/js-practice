import Backpack from "./Backpack.js";

const everydayPack = new Backpack(
  "Everyday Backpack",
  30,
  "grey",
  15,
  26,
  26,
  true
);

const content = `
    <h1 class="backpack__name">${everydayPack.name}</h1>
    <ul class="backpack__features">
      <li class="packprop backpack__volume">Volume:<span> ${everydayPack.volume}</span></li>
      <li class="packprop backpack__color">Color:<span> ${everydayPack.color}</span></li>
      <li class="packprop backpack__pockets">Number of pockets:<span> ${everydayPack.pocketNum}</span></li>
      <li class="packprop backpack__strap">Left strap length:<span> ${everydayPack.strapLength.left} inches</span></li>
      <li class="packprop backpack__strap">Right strap length:<span> ${everydayPack.strapLength.right} inches</span></li>
      <li class="packprop backpack__lid">Lid status:<span> ${everydayPack.lidOpen}</span></li>
    </ul>
  
`;

const main = document.querySelector(".maincontent");

const newArticle = document.createElement("article");

newArticle.innerHTML = content;

main.append(newArticle);
