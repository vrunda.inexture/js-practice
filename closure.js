const myButton = document.getElementById("myButton");
const myText = document.getElementById("myText");

let clicked = 0;

myButton.addEventListener("click", function handleClick() {
  clicked++;
  myText.innerText = `You clicked ${clicked} times`;
});
