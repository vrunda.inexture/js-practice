const personPrototype = {
  greet() {
    console.log(`hello, my name is ${this.name}!`);
  },
};

function Person(name) {
  this.name = name;
}

Person.prototype = personPrototype;
Person.prototype.constructor = Person;

const vrunda = new Person("vrunda");
vrunda.greet(); // hello, my name is vrunda!
