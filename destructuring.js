//destructuring, spread and rest
function getStudents(classroom) {
  let { hasTechAssistant, classList } = classroom;
  let teacher, techAssistant, students;

  if (hasTechAssistant) {
    [teacher, techAssistant, ...students] = classList;
  } else {
    [teacher, ...students] = classList;
  }
  return students;
}

console.log(
  getStudents({
    hasTechAssistant: true,
    classList: ["Vrunda", "Manan", "Bhargav", "Kinjal"],
  })
);
