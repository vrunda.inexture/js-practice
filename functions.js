window.volume = 50;

const FirstBook = {
  name: "Untamed Dreams",
  author: "M.Firechild",
  Price: 500,
  volume: 42,
  newVolume: function (volume) {
    console.log("Volume :", this.volume);
    this.volume = volume;
    console.log("Changed Volume: ", this.volume);

    //funtion declartion global scope
    (function () {
      console.log("Volume inside a function:", this.volume);
    })();

    //arrow function with nearest scope
    (() => {
      console.log("Volume inside arrow function:", this.volume);
    })();
  },
};

console.log(FirstBook.newVolume(20));
